﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExample
{
    class Vehicle
    {
        public Vehicle()
        {
            Console.WriteLine("Vehicle constructed");
        }

        ~Vehicle()
        {
            Console.WriteLine("Vehicle destructed");
        }

        public virtual int GetNumWheels()
        {
            return 0;
        }
    }
}

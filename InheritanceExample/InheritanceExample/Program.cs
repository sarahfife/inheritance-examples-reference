using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            ProcessVehicles();
            System.GC.Collect();
            Console.ReadKey();
        }

        static void ProcessVehicles()
        {
            Console.WriteLine("Declaring a Vehicle");
            Vehicle myVehicle = new Vehicle();

            Console.WriteLine("Declaring a Car");
            Car myCar = new Car();

            Console.WriteLine("GetNumWheels() for Vehicle: " + myVehicle.GetNumWheels());
            Console.WriteLine("GetNumWheels() for Car: " + myCar.GetNumWheels());

            Console.WriteLine("Destroying all Vehicles");
        }
    }
}
